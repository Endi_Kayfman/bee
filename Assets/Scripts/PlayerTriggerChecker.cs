using System.Collections.Generic;
using System.Security;
using UnityEngine;

public class PlayerTriggerChecker : MonoBehaviour
{
    [SerializeField] private BoidSeparation _firstBee = null;

    public List<BoidSeparation> collectedBees = new List<BoidSeparation>();

    private void Start()
    {
        collectedBees.Add(_firstBee);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Seek seek))
        {
            if (!seek.isActiveAndEnabled)
            {
                seek.enabled = true;
                seek.GetComponent<Face>().enabled = true;
                
                seek.gameObject.transform.parent = null;

                BoidSeparation boid = seek.GetComponent<BoidSeparation>();
                collectedBees.Add(boid);
                
                AddBee();
            }
        }
    }

    private void AddBee()
    {
        foreach (var bee in collectedBees)
        {
            bee.targets = collectedBees;
        }
    }
}
