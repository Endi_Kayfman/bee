﻿using System.Collections.Generic;
using UnityEngine;

public class BoidSeparation : Flee
{
    public float desiredSeparation = 15.0f;
    public List<BoidSeparation> targets;

    public override Steering GetSteering()
    {
        Steering steer = new Steering();
        int count = 0;
        
        foreach(BoidSeparation other in targets)
        {
            if (other != null)
            {
                float distance = (transform.position - other.transform.position).magnitude;
                
                if((distance > 0) && (distance < desiredSeparation))
                {
                    Vector3 diff = transform.position - other.transform.position;
                    diff.Normalize();
                    diff /= distance;
                    steer.Linear += diff;
                    count++;
                }
            }
        }

        return steer;
    }
}
