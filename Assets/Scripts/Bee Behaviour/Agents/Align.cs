using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Align : AgentBehavior
{
   public float targetRadius;
   public float slowRadius;
   public float TimeToTarget = 0.1f;

   public override Steering GetSteering()
   {
      Steering steering = new Steering();
      float targetOrientation = target.GetComponent<Agent>().orientation;
      float rotation = targetOrientation - agent.orientation;
      
      rotation = MapToRange(rotation);
      float rotationSize = Mathf.Abs(rotation);
      
      if (rotationSize < targetRadius)
      {
         return steering;
      }

      float targetRotation;

      if (rotationSize > slowRadius)
      {
         targetRotation = agent.maxRotation;
      }
      else
      {
         targetRotation = agent.maxRotation * rotationSize / slowRadius;
      }

      targetRotation *= rotation / rotationSize;

      steering.Angular = targetRotation - agent.rotation;
      steering.Angular /= TimeToTarget;
      float angularAccel = Mathf.Abs(steering.Angular);

      if (angularAccel > agent.maxAngularAccel)
      {
         steering.Angular /= angularAccel;
         steering.Angular *= agent.maxAngularAccel;
      }

      return steering;
   }
}
