﻿using UnityEngine;
using UnityEngine.PlayerLoop;

public class Seek : AgentBehavior
{
    public override Steering GetSteering()
    {
        Steering steer = new Steering();
        steer.Linear = target.transform.position - transform.position;
        steer.Linear.Normalize();
        steer.Linear = steer.Linear * agent.maxAccel;
        
        return steer;
    }
    
}
