﻿using UnityEngine;

public class Steering
{
    public float Angular = 0f;
    public Vector3 Linear;
    public Steering()
    {
        Angular = 0.0f;
        Linear = new Vector3();
    }
}
