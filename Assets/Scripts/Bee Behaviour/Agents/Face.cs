using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Face : Align
{
    [SerializeField]
    private GameObject targetAux;
    
    public override Steering GetSteering()
    {
        Vector3 direction = targetAux.transform.position - transform.position ;
        
        if (direction.magnitude > 3f)
        {
            float targetOrientation = Mathf.Atan2(direction.x, direction.z);
            targetOrientation *= Mathf.Rad2Deg;
            target.GetComponent<Agent>().orientation = targetOrientation;
        }
        else
        {
            target.GetComponent<Agent>().orientation = 0;
        }

        return base.GetSteering();
    }
}
