﻿using UnityEngine;

public class AgentBehavior : MonoBehaviour
{
    public float weight = 1.0f;

    public GameObject target = null;
    //public GameObject beeChild = null;
    protected Agent agent = null;
    private Rigidbody _rigidbody = null;

    //rotation
    // public float maxSpeed;
    // public float maxAccel;
    // public float maxRotation;
    // public float maxAngularAccel;

    public virtual void Awake()
    {
        agent = gameObject.GetComponent<Agent>();
        _rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    public virtual void Update()
    {
        agent.SetSteering(GetSteering(), weight);
    }

    public virtual Steering GetSteering()
    {
        return new Steering();
    }

    public float MapToRange(float rotation)
    {
        rotation %= 360f;

        if (Mathf.Abs(rotation) > 180f)
        {
            if (rotation < 0f)
            {
                rotation += 360f;
            }
            else
            {
                rotation -= 360f;
            }
        }
        return rotation;
    }
}
