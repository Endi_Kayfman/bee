﻿using UnityEngine;
using UnityEngine.Assertions.Must;

public class Agent : MonoBehaviour
{
    public float maxSpeed = 1.0f;
    public float maxAccel = 30.0f;
    public float maxRotation;
    public float maxAngularAccel;

    public float orientation = 0f;
    public float rotation = 0f;
    private Vector3 velocity = Vector3.zero;
    private Steering steer = null;

    void Start()
    {
        velocity = Vector3.zero;
        steer = new Steering();
    }

    public void SetSteering(Steering steer, float weight)
    {
        this.steer.Linear += (weight * steer.Linear);
        this.steer.Angular += (weight * steer.Angular);
    }
    
    public virtual void Update()
    {
        Vector3 displacement = velocity * Time.deltaTime;
        displacement.y = 0;

        orientation += rotation * Time.deltaTime;
        
        if(orientation < 0.0f)
        {
            orientation += 360.0f;
        }
        else if(orientation > 360.0f)
        {
            orientation -= 360.0f;
        }
        
        transform.Translate(displacement, Space.World);
        transform.rotation = new Quaternion();
        transform.Rotate(Vector3.up, orientation);
    }
    
    public virtual void LateUpdate()
    {
        velocity += steer.Linear * Time.deltaTime;
        rotation += steer.Angular * Time.deltaTime;
        
        if(velocity.magnitude > maxSpeed)
        {
            velocity.Normalize();
            velocity = velocity * maxSpeed;
        }

        if(steer.Linear.magnitude == 0.0f)
        {
            velocity = Vector3.zero;
        }
        steer = new Steering();
    }
}
