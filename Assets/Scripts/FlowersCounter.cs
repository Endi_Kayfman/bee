using UnityEngine;
using UnityEngine.UI;

public class FlowersCounter : MonoBehaviour
{
    #region Singleton

    public static FlowersCounter Instance;

    private void Awake()
    {
        Instance = this;
    }

    #endregion
    
    [SerializeField] private Text textCounter = null;

    public int flowersCounter = 0;

    public void AddFlowers()
    {
        flowersCounter++;
    }

    public void SubstractCost(int cost)
    {
        if (flowersCounter > 0)
        {
            flowersCounter -= cost;
        }
    }
    
    public void UpdateText()
    {
        textCounter.text = "Collected Flowers: " + flowersCounter;
    }
}
