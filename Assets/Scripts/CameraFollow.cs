using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform playerTransform = null;

    [SerializeField] [Range(0f ,10f)] private float smoothSpeed = 0;

    [SerializeField] private Vector3 offset = Vector3.zero;

    void FixedUpdate()
    {
        Vector3 desiredPosition = new Vector3(playerTransform.position.x,  transform.position.y, playerTransform.position.z) + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
        transform.position = smoothedPosition;
    }
}