using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlowerController : MonoBehaviour
{
    [SerializeField] private Image _healthBar = null;
    [SerializeField] private int _flowerHealth = 100;
    [SerializeField] private int _maxAllowedBees = 5;
    private bool _isFlowerBusy = false;
    
    [SerializeField] private PlayerTriggerChecker player;
    
    private List<Seek> tempBeeList = new List<Seek>();

    private void Start()
    {
        _healthBar.fillAmount = _flowerHealth / 100f;
        _healthBar.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Seek bee))
        {
            if (!_isFlowerBusy)
            {
                _healthBar.gameObject.SetActive(true);
                _healthBar.transform.LookAt(Camera.main.transform.position);

                if (tempBeeList.Count == 0)
                {
                    tempBeeList.Add(bee);
                    StartCoroutine(BeeCollectingCor(bee));
                }
                else
                {
                    tempBeeList.Add(bee);
                }

                if (tempBeeList.Count == _maxAllowedBees)
                {
                    _isFlowerBusy = true;
                }

                bee.gameObject.GetComponent<Face>().enabled = false;
                bee.target = gameObject;
                
                //bee.GetComponentInChildren<ParticleSystem>().Play();
                bee.GetComponent<Animator>().SetTrigger("Working");
            }
        }
    }

    private IEnumerator BeeCollectingCor(Seek bee)
    {
        while (_flowerHealth > 0)
        {
            yield return new WaitForSeconds(1f);
            _flowerHealth -= 5 * tempBeeList.Count;

            _healthBar.fillAmount = _flowerHealth / 100f;
        }

        foreach (var tempBee in tempBeeList)
        {
            tempBee.target = player.gameObject;
            tempBee.GetComponent<Animator>().SetTrigger("Idle");
            tempBee.gameObject.GetComponent<Face>().enabled = true;
        }
        
        FlowersCounter.Instance.AddFlowers();
        FlowersCounter.Instance.UpdateText();
        
        tempBeeList.Clear();
        
        gameObject.SetActive(false);
    }
}
