using UnityEngine;

public class UIController : MonoBehaviour
{
   [SerializeField] private GameObject menu = null;
   [SerializeField] private GameObject beePrefab = null;

   [SerializeField] private PlayerMovement playerMovement;
   [SerializeField] private PlayerTriggerChecker playerTriggerChecker;

   [SerializeField] private int costToAddBee = 3;
   [SerializeField] private int costToAddSpeed = 3;
   [SerializeField] private int increaseSpeedValue = 5;
   private GameObject beeTemp = null;
   
   private void Start()
   {
      menu.SetActive(false);
   }

   public void OpenMenu()
   {
      menu.SetActive(true);
   }
   
   public void CloseMenu()
   {
      menu.SetActive(false);
   }

   public void AddBees()
   {
      if (FlowersCounter.Instance.flowersCounter >= costToAddBee)
      {
         FlowersCounter.Instance.SubstractCost(costToAddBee);
         FlowersCounter.Instance.UpdateText();
         beeTemp = Instantiate(beePrefab);
         playerTriggerChecker.collectedBees.Add(beeTemp.GetComponent<BoidSeparation>());

         //costToAddBee += 1;
      }
     
   }

   public void AddSpeed()
   {
      if (FlowersCounter.Instance.flowersCounter >= costToAddSpeed)
      {
         FlowersCounter.Instance.SubstractCost(costToAddBee);
         FlowersCounter.Instance.UpdateText();
         playerMovement.IncreaseSpeed(increaseSpeedValue);

         foreach (var bee in playerTriggerChecker.collectedBees)
         {
            bee.GetComponent<Agent>().maxSpeed += 2f;
         }

         //costToAddSpeed += 1;
      }
   }
   
}
