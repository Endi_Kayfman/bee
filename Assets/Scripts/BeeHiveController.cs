using UnityEngine;

public class BeeHiveController : MonoBehaviour
{
   [SerializeField] private UIController uiController = null;
   
   private void OnTriggerEnter(Collider other)
   {
      Debug.Log("entered");
      
      if (other.TryGetComponent(out PlayerMovement player))
      {
         uiController.OpenMenu();
      }
   }
   
   private void OnTriggerExit(Collider other)
   {
      Debug.Log("exit");
      if (other.TryGetComponent(out PlayerMovement player))
      {
         uiController.CloseMenu();
      }
   }
}
